import { SET_ERROR, SET_LOADING } from './consts';

const initialState = {
  isLoading: false,
  error: null
};

export const statusReducer = (prefix) => (state = initialState, action) => {
  switch (action.type) {
    case `${prefix}_${SET_LOADING}`:
      return {
        ...state,
        isLoading: action.payload,
        error: action.payload ? null : state.error
      }
    case `${prefix}_${SET_ERROR}`:
      return {
        ...state,
        error: action.payload
      }
    default:
      return state;
  }
}