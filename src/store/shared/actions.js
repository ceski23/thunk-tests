export const createAction = (type, payload, prefix) => ({
  type: prefix ? `${prefix}_${type}` : type,
  payload,
});