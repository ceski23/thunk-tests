import { createStore, combineReducers, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunkMiddleware from 'redux-thunk'
import users from 'store/users/reducer'
import createDebounce from 'redux-debounced';

export const rootReducer = combineReducers({
  users
});

export const store = createStore(rootReducer, composeWithDevTools(
  applyMiddleware(createDebounce(), thunkMiddleware)
));