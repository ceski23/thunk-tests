import { combineReducers } from 'redux';

import { paginationReducer } from 'store/shared/pagination/reducer';
import { statusReducer } from 'store/shared/status/reducer';
import { GET_USERS, PREFIX } from './consts';

const initialState = {
  users: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_USERS:
      return {
        ...state,
        users: action.payload
      }
    default:
      return state;
  }
};

export default combineReducers({
  data: reducer,
  pagination: paginationReducer(PREFIX),
  status: statusReducer(PREFIX)
});