import { GET_USERS, PREFIX } from './consts';
import { createAction } from 'store/shared/actions';
import { SET_ERROR, SET_LOADING } from 'store/shared/status/consts';
import * as api from './api';
import { SET_PAGINATION, CHANGE_PAGE } from 'store/shared/pagination/consts';

export const getUsers = (data) => createAction(GET_USERS, data);
export const setLoading = (loading) => createAction(SET_LOADING, loading, PREFIX);
export const setError = (error) => createAction(SET_ERROR, error, PREFIX);
export const setPagination = (data) => createAction(SET_PAGINATION, data, PREFIX);
export const changePage = (page) => createAction(CHANGE_PAGE, page, PREFIX);

export const fetchUsers = () => (dispatch, getState) => {
  const { users: { pagination: { currentPage }} } = getState();
  dispatch(setLoading(true));
  return api.getUsers({ page: currentPage })
    .then(({ _meta, result }) => {
      dispatch(setPagination(_meta));
      dispatch(getUsers(result));
      dispatch(setLoading(false));
      return result;
    })
    .catch(({ message }) => {
      dispatch(setError(message));
      dispatch(setLoading(false));
    });
};