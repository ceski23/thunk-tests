import client from 'utils/apiClient';

export const getUsers = ({ page } = {}) => client.get('users', {
  params: {
    page
  }
}).then(({data}) => data);

export const getUser = (id) => client
  .get(`users/${id}`)
  .then(({data}) => data);