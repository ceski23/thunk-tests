import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import styled from 'styled-components';

import { fetchUsers, changePage } from 'store/users/actions';
import { Users } from 'ui/components/Users';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  /* background: #2c2c2c; */
  height: 100%;
  /* color: white; */
`;

const Actions = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  height: 80px;
`;

const Loading = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100%;
  font-size: 2em;
`;

const Button = styled.button`
  padding: 10px 20px;
  /* background: #ff7600; */
  /* border: none; */
  margin: 0 20px;
  height: 40px;
`;

const App = () => {
  const dispatch = useDispatch();
  const { data, status, pagination } = useSelector(state => state.users);

  useEffect(() => {
    dispatch(fetchUsers()).then(users => {
      console.log(`There are ${users.length} users`);
    });
  }, [dispatch]);

  const handleNextClick = () => {
    dispatch(changePage(pagination.currentPage + 1));
    dispatch(fetchUsers());
  };

  const handlePrevClick = () => {
    dispatch(changePage(pagination.currentPage - 1));
    dispatch(fetchUsers());
  };

  return (
    <Container>
      {status.isLoading && <Loading>Loading...</Loading>}
      {status.error && !status.isLoading && <p>{status.error}</p>}
      {!status.isLoading && <Users users={data.users} />}
      <Actions>
        <Button onClick={handlePrevClick}>PREV PAGE</Button>
        <h4>{pagination.currentPage}</h4>
        <Button onClick={handleNextClick}>NEXT PAGE</Button>
      </Actions>
    </Container>
  );
}

export default App;
