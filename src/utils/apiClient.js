import axios from 'axios';

const client = axios.create({
  baseURL: 'https://gorest.co.in/public-api/',
  contentType: 'application/json',
});

client.defaults.headers.common['Authorization'] = 'Bearer M8hOyEDIzNoLcIiHl0ORzdU4rW9a5EZB0Se0';

client.interceptors.response.use(
  response => response, 
  error => {
    alert(error.message);
    return Promise.reject(error);
  }
);

export default client;