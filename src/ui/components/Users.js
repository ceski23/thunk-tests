import React from 'react';
import styled from 'styled-components';
import { User } from './User';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  overflow: auto;
  flex: 1;
`;

export const Users = ({ users }) => (
  <Container>
    {users.map(user => <User user={user} />)}
  </Container>
)