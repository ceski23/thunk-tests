import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin: 10px 20px;
`;

const Avatar = styled.img`
  width: 40px;
  height: 40px;
  border-radius: 50%;
`;

const Name = styled.p`
  margin: 0;
  margin-left: 20px;
`;

export const User = ({ user }) => (
  <Container>
    <Avatar src={user._links.avatar.href} alt="" />
    <Name>{user.first_name} {user.last_name}</Name>
  </Container>
);